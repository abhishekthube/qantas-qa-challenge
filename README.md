# Role - Qantas Loyalty Quality Engineer

## Instructions for running the test suites :


## REST API 

Go to URL : https://qa-challenges-lightbulb.atlassian.io

Create an automated regression for the below ACs

Using either RestAssure or Newman

API documentation https://qa-challenges-lightbulb.atlassian.io/api/allmethods

- AC1. I want to turn on and off my light

Provide a reports of the exucution and any bugs found ( be creative )

### Steps to execute tests

Clone the repo. 

Api tests are located in the 'ApiTests' directory.

Go to ApiTests directory on local, Shift right click in the same directory and open command window

Run the below command on command prompt

"newman run LightBulbTests.newman_collection.json --reporters html"

The above command will run the tests and generate report in html file in the newman directory.


### Test result and defect

A test report has been checked in "newman-run-report-2018-07-21-07-59-44-972-0.html".

5/6 tests passed
1 test faied = SwitchOnPowerAs0

Defect details :
Summary : 
	Switch on api call with a power input of 0 returns a status code of 200.
	
Description : 

	Switch on api call with an input of power = 0 returns a OK (200) status code which is not as expected. 
	
	This fails the boundary value analysis test as we are expecting a bad request, status 400 in response.


----------------------------------------------------------------------------------------------------------------------------------------


## WEB Automation

Go to URL : http://phptravels.com/demo/

Create an automated smoke test suite using http://devexpress.github.io/testcafe/ or your framework of choice for the below ACs 
(you can do some manual set up, but this must be documented)

- AC1. I want to be able book hotels, flights or tours

Provide a reports of the exucution and any bugs found ( be creative )


### Steps to execute tests

PhpTravels project includes smoke tests for the application for booking flight, tour and hotel.

A launcher has been provided RunPhpTravelsTest.launch. Import this into eclipse "File > Import... > Run/Debug > Launch Configurations" and run. After successful execution, html report will be created in the dir "PhpTravels\target\site".

An execution report has also been added to PhpTravels\Smoke-Test-report.html which indicates the successful execution of the 3 smoke tests.

### Bug noticed during the automation task :

#### Summary : Total number of nights on hotel booking page not calculated properly

#### Description : 

Steps to reproduce ->

Enter City 

Enter check in and check out dates

Enter guests

Click search and wait for next page to load

On the Hotel booking page, check the number against "NIGHTS" label in the "Refine dates" section. The number always remains 1 in spite of variable checkin checkout dates.