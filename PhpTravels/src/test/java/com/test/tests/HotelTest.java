package com.test.tests;

import org.junit.Assert;
import org.junit.Test;

import com.test.pages.HomePage;
import com.test.pages.HotelBookingPage;
import com.test.pages.BookingConfirmationPage;
import com.test.pages.InvoicePage;
import com.test.pages.HotelResultsPage;

public class HotelTest extends BaseTest {

	HomePage homePage = new HomePage(driver);
	HotelResultsPage hotelResultsPage = new HotelResultsPage(driver);
	HotelBookingPage hotelBookingPage = new HotelBookingPage(driver);
	BookingConfirmationPage hotelConfirmBookingPage = new BookingConfirmationPage(driver);
	InvoicePage hotelInvoicePage = new InvoicePage(driver);
	
	@Test
	public void hotelBookingSmokeTest() {
		
		homePage.selectMainCategory("Hotels");
		homePage.setCity("Singapore");
		homePage.setCheckinDate("10/10/2018");
		homePage.setCheckoutDate("20/10/2018");
		homePage.setTravellerNumbers("2 Adult 1 Child");
		homePage.clickSearch();
		
		//check if results include hotel to be booked
		Assert.assertEquals("Hotel not found in results", true, hotelResultsPage.checkHotelInResults("Rendezvous Hotels"));
		
		hotelResultsPage.clickDetailsButton("Rendezvous Hotels");
		
		//check if hotel booking page opened
		Assert.assertEquals("Hotel booking page did not open", true, hotelBookingPage.checkBookingPageHeader("Rendezvous Hotels"));
		
		//check if a certain room is present on the page to book
		Assert.assertEquals("Room does not exist", true,hotelBookingPage.checkIfRoomExists("Junior Suites"));
		
		hotelBookingPage.clickBookNow("Junior Suites");
		
		Assert.assertEquals("Confirm booking page did not open",true, hotelConfirmBookingPage.checkIfConfirmBookingOpened());
		
		hotelConfirmBookingPage.setFirstName("Abhishek");
		hotelConfirmBookingPage.setLastName("Thube");
		hotelConfirmBookingPage.setEmail("abc.cba@gmail2.com");
		hotelConfirmBookingPage.setConfirmEmail("abc.cba@gmail2.com");
		hotelConfirmBookingPage.clickConfirmBooking();
		
		//check if invoice is generated
		Assert.assertEquals("Invoice did not get generated",true, hotelInvoicePage.checkIfInvoiceGenerated());
		
	}

}
