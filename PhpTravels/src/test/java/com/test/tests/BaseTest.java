package com.test.tests;

import java.util.concurrent.TimeUnit;

import org.junit.After;
//import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BaseTest {

	public static WebDriver driver;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.setProperty("webdriver.chrome.driver", "C:\\Projects\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("https://www.phptravels.net/");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		driver = null;
	}

	@Before
	public void setUp() throws Exception {
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
	}
	@After
	public void teardown() throws Exception {
		driver.quit();
	}

}
