package com.test.tests;


import org.junit.Assert;
import org.junit.Test;

import com.test.pages.BookingConfirmationPage;
import com.test.pages.HomePage;
import com.test.pages.InvoicePage;
import com.test.pages.TourBookingPage;

public class ToursTest extends BaseTest {

	HomePage homePage = new HomePage(driver);
	BookingConfirmationPage bookingConfirmationPage = new BookingConfirmationPage(driver);
	InvoicePage invoicePage = new InvoicePage(driver);
	TourBookingPage tourBookingPage = new TourBookingPage(driver);
	
	@Test
	public void tourBookingSmokeTest() {
		
		homePage.selectMainCategory("Tours");
		homePage.setTourCity("Syd");
		homePage.setTourDate("10/10/2018");
		homePage.selectGuests("1 Guests");
		homePage.setTourType("Adventure");
		homePage.clickTourSearch();

		Assert.assertEquals("Tour booking page did not open", true, tourBookingPage.checkBookingPageHeader("Sydney"));

		tourBookingPage.clickBookNow();
		
		Assert.assertEquals("Confirm booking page did not open",true, bookingConfirmationPage.checkIfConfirmBookingOpened());
		
		bookingConfirmationPage.setFirstName("Abhishek");
		bookingConfirmationPage.setLastName("Thube");
		bookingConfirmationPage.setEmail("abc.cba@gmail2.com");
		bookingConfirmationPage.setConfirmEmail("abc.cba@gmail2.com");
		bookingConfirmationPage.clickConfirmBooking();
		
		//check if invoice is generated
		Assert.assertEquals("Invoice did not get generated",true, invoicePage.checkIfInvoiceGenerated());
	
		
	}
}
