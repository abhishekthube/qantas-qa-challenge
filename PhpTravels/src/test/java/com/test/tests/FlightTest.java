package com.test.tests;

import org.junit.Test;
import org.junit.Assert;

import com.test.pages.BookingConfirmationPage;
import com.test.pages.FlightAvailabilityPage;
import com.test.pages.HomePage;
import com.test.pages.InvoicePage;


public class FlightTest extends BaseTest {

	HomePage homePage = new HomePage(driver);
	FlightAvailabilityPage flightAvailibilityPage = new FlightAvailabilityPage(driver);
	BookingConfirmationPage bookingConfirmationPage = new BookingConfirmationPage(driver);
	InvoicePage invoicePage = new InvoicePage(driver);
	
	@Test
	public void flightBookingSmokeTest() {
		
		homePage.selectMainCategory("Flights");
		homePage.setFromCity("Kingsford");
		homePage.setToCity("Changi");
		homePage.setDepartureDate("2018-10-10");
		homePage.clickFlightSearch();
		
		//check if the flight availability page is displayed
		Assert.assertEquals("Flight avilability page not displayed", true, flightAvailibilityPage.checkFlightAvailbilityPageLoaded());
	
		flightAvailibilityPage.clickBookNow();
		
		Assert.assertEquals("Confirm booking page did not open",true, bookingConfirmationPage.checkIfConfirmBookingOpened());
		
		bookingConfirmationPage.setFirstName("Abhishek");
		bookingConfirmationPage.setLastName("Thube");
		bookingConfirmationPage.setEmail("abc.cba@gmail2.com");
		bookingConfirmationPage.setConfirmEmail("abc.cba@gmail2.com");
		bookingConfirmationPage.clickConfirmBooking();
		
		//check if invoice is generated
		Assert.assertEquals("Invoice did not get generated",true, invoicePage.checkIfInvoiceGenerated());
		
		
	}

}
