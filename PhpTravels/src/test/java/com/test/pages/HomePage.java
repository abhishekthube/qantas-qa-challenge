package com.test.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage extends BasePage {

	public HomePage(WebDriver driver) {
		super(driver);
	}

	public void selectMainCategory(String category) {
		String xpathCategory="";
		switch (category.toLowerCase()) {
		case "hotels" :
			xpathCategory = "//span[@class='hidden-xs' and contains(text(), 'Hotels')]";
			break;
		case "flights" :
			xpathCategory = "//span[@class='hidden-xs' and contains(text(), 'Flights')]";
			break;
		case "tours" :
			xpathCategory = "//span[@class='hidden-xs' and contains(text(), 'Tours')]";
			break;
		default:
			System.out.println("Category name invalid");
		}
		driver.findElement(By.xpath(xpathCategory)).click();
	}

	public void setCity(String city){
		driver.findElement(By.cssSelector("a[class='select2-choice']")).click();
		driver.findElement(By.cssSelector("#select2-drop > div > input")).sendKeys(city);
		driver.findElement(By.xpath("//div[@class='select2-result-label' and contains(text() , '" + city + "')]")).click();				
	}
	

	public void setCheckinDate(String checkinDate){
		driver.findElement(By.name("checkin")).sendKeys(checkinDate);
	}

	public void setCheckoutDate(String checkoutDate){
		driver.findElement(By.name("checkout")).sendKeys(checkoutDate);
	}

	public void setTravellerNumbers(String travellerNumbers){
		driver.findElement(By.id("travellersInput")).clear();
		driver.findElement(By.id("travellersInput")).sendKeys(travellerNumbers);
	}
	
	public void clickSearch() {
		
		driver.findElement(By.cssSelector("button[class='btn btn-lg btn-block btn-danger pfb0 loader']")).click();
	}
	
	public void clickFlightSearch() {
		driver.findElement(By.cssSelector("button[class='btn-danger btn btn-lg btn-block pfb0']")).click();
	}
	
	public void setFromCity(String city) {
		this.waitForElementToLoad(By.cssSelector("#s2id_location_from > a"));
		this.scrollToElement(driver.findElement(By.cssSelector("#s2id_location_from > a")));
		driver.findElement(By.cssSelector("#s2id_location_from > a")).click();
		driver.findElement(By.cssSelector("#select2-drop > div > input")).sendKeys(city);
		this.waitForElementToLoad(By.xpath("//span[@class='select2-match' and contains(text() , '" + city + "')]"));
		driver.findElement(By.xpath("//span[@class='select2-match' and contains(text() , '" + city + "')]")).click();				
	}
	
	public void setToCity(String city) {
		this.waitForElementToLoad(By.cssSelector("#s2id_location_to > a"));
		driver.findElement(By.cssSelector("#s2id_location_to > a")).click();
		driver.findElement(By.cssSelector("#select2-drop > div > input")).sendKeys(city);
		this.waitForElementToLoad(By.xpath("//span[@class='select2-match' and contains(text() , '" + city + "')]"));
		driver.findElement(By.xpath("//span[@class='select2-match' and contains(text() , '" + city + "')]")).click();				
	}

	public void setDepartureDate(String departureDate){
		driver.findElement(By.name("departure")).sendKeys(departureDate);
	}
	
	public void setTourCity(String city) {
		this.waitForElementToLoad(By.cssSelector("#s2id_autogen10 > a"));
		this.scrollToElement(driver.findElement(By.cssSelector("#s2id_autogen10 > a")));
		driver.findElement(By.cssSelector("#s2id_autogen10 > a")).click();
		driver.findElement(By.cssSelector("#select2-drop > div > input")).sendKeys(city);
		this.waitForElementToLoad(By.xpath("//span[@class='select2-match' and contains(text() , '" + city + "')]"));
		driver.findElement(By.xpath("//span[@class='select2-match' and contains(text() , '" + city + "')]")).click();				

	}
	public void setTourDate(String tourDate){
		driver.findElement(By.name("date")).clear();
		driver.findElement(By.name("date")).sendKeys(tourDate);
		driver.findElement(By.name("date")).click();
	}
	
	public void selectGuests(String noOfGuests) {
		this.selectFromDropDown(By.id("adults"), noOfGuests);
	}

	public void setTourType(String tourType) {
		driver.findElement(By.cssSelector("#s2id_tourtype > a")).click();;
		driver.findElement(By.cssSelector("#select2-drop > div > input")).sendKeys(tourType);
		this.waitForElementToLoad(By.xpath("//span[@class='select2-match' and contains(text() , '" + tourType + "')]"));
		driver.findElement(By.xpath("//span[@class='select2-match' and contains(text() , '" + tourType + "')]")).click();				

	}

	public void clickTourSearch() {
		driver.findElement(By.cssSelector("button[class='btn-danger btn btn-lg btn-block pfb0 loader']")).click();
//		driver.findElement(By.xpath("//*[@id=\"TOURS\"]/form/div[5]/button")).click();
		
	}
	
	
}
