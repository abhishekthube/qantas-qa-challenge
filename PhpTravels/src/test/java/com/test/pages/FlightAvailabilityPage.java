package com.test.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class FlightAvailabilityPage extends BasePage {

	public FlightAvailabilityPage(WebDriver driver) {
		super(driver);
	}

	
	public Boolean checkFlightAvailbilityPageLoaded() {
		return this.isElementPresentOnPage(By.xpath("//div[@class='panel-heading' and contains(text() , 'Available Flights')]"));
		
	}
	
	public void clickBookNow() {
		driver.findElement(By.xpath("(//button[@id='bookbtn'])[1]")).click();
	}
}
