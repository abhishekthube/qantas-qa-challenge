package com.test.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HotelBookingPage extends BasePage {

	public HotelBookingPage(WebDriver driver) {
		super(driver);
	}

	public Boolean checkBookingPageHeader(String hotelName) {
		return this.isElementPresentOnPage(By.xpath("//span[contains(text(),'" + hotelName + "')]"));
	}
		
	public Boolean checkIfRoomExists(String roomType) {
		return this.isElementPresentOnPage(By.xpath("//div[@class='col-md-10 col-xs-7 g0-left']//*[contains(text(),'" + roomType +"')]"));
		
	}

	public void clickBookNow(String roomType) {
		this.scrollToElement(driver.findElement(By.xpath("//div[@class='col-md-10 col-xs-7 g0-left']//*[contains(text(),'" + roomType +"')]//following::button[text()='Book Now'][1]")));
		driver.findElement(By.xpath("//div[@class='col-md-10 col-xs-7 g0-left']//*[contains(text(),'" + roomType +"')]//following::button[text()='Book Now'][1]")).click();
	}
}
