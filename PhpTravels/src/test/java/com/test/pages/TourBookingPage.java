package com.test.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class TourBookingPage extends BasePage {

	public TourBookingPage(WebDriver driver) {
		super(driver);
	}

	public Boolean checkBookingPageHeader(String tourName) {
		return this.isElementPresentOnPage(By.xpath("//span[contains(text(),'" + tourName + "')]"));
	}
		
	public void clickBookNow() {
		this.scrollToElement(driver.findElement(By.cssSelector("button[class='btn btn-block btn-action btn-lg loader']")));
		driver.findElement(By.cssSelector("button[class='btn btn-block btn-action btn-lg loader']")).click();
	}
}
