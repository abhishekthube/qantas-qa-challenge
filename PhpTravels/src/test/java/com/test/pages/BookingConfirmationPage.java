package com.test.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class BookingConfirmationPage extends BasePage {

	public BookingConfirmationPage(WebDriver driver) {
		super(driver);
		}

	public Boolean checkIfConfirmBookingOpened() {
		return this.isElementPresentOnPage(By.cssSelector("button[class='btn btn-success btn-lg btn-block completebook']"));
	}
	
	public void clickConfirmBooking() {
		driver.findElement(By.cssSelector("button[class='btn btn-success btn-lg btn-block completebook']")).click();
	}
	
	public void setFirstName(String firstName) {
		driver.findElement(By.name("firstname")).sendKeys(firstName);
	}
	
	public void setLastName(String lastName) {
		driver.findElement(By.name("lastname")).sendKeys(lastName);
	}

	public void setEmail(String email) {
		driver.findElement(By.name("email")).sendKeys(email);
	}

	public void setConfirmEmail(String confirmEmail) {
		driver.findElement(By.name("confirmemail")).sendKeys(confirmEmail);
	}
}
