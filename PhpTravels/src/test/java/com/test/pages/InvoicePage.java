package com.test.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class InvoicePage extends BasePage {

	public InvoicePage(WebDriver driver) {
		super(driver);
	}

	public Boolean checkIfInvoiceGenerated() {
		return this.isElementPresentOnPage(By.xpath("//div[text()='Invoice']"));
	}
}
