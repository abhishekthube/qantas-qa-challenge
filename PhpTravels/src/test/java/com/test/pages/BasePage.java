package com.test.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {

	protected WebDriver driver;
	BasePage(WebDriver driver){
		this.driver = driver;
	}

	public void selectFromDropDown(By objDesc, String valueToSelect) {
		Select list = new Select(driver.findElement(objDesc));
		list.selectByVisibleText(valueToSelect);
		
	}
	public Boolean waitForElementToLoad(By objDesc) {
		WebDriverWait w = new WebDriverWait(driver, 10);
		try {
			w.until(ExpectedConditions.presenceOfElementLocated(objDesc));
		}
		catch(TimeoutException t) {
			return false;
		}
		return true;
	}
	
	public Boolean isElementPresentOnPage(By objDesc) {
		List<WebElement> listObj = driver.findElements(objDesc);
		if (listObj.size() == 0) {
//			System.out.println("obj not found");
			return false;
		}
//		System.out.println("obj found");
		return true;
	}
	
	public void scrollToElement(WebElement objDesc) {
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();", objDesc);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}
	}
}
