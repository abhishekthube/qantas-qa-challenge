package com.test.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HotelResultsPage extends BasePage {

	public HotelResultsPage(WebDriver driver) {
		super(driver);
	}

	public Boolean checkHotelInResults(String hotelName) {
		return this.isElementPresentOnPage(By.linkText(hotelName));
	}
	
	public void clickDetailsButton(String hotelName) {
		driver.findElement(By.linkText(hotelName)).click();
	}
}
